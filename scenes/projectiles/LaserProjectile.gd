extends Area

var velocity = Vector2.ZERO
var dmg = 10
var parent

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	global_translate(velocity*delta)


func _on_Area_body_entered(body):
	if body.has_method("damage") && body.is_network_master() && body.get_network_master() != parent:
		body.damage(dmg)
		rpc("destroy")

remotesync func destroy():
	queue_free()
