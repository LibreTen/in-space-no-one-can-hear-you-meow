extends Resource
class_name Ship

export var max_players = 1 # == number of stations
export var linear_damp = 0.5
export var angular_damp = 0.5
export var accel_base = 200
export var accel_booster = 3
export var stations = {
	"piloting":{
		"radar":false,
		"weapons":false,
		"autopilot":false
	}
}



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
