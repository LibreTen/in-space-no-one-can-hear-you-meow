extends Spatial

onready var laser_projectile = preload("res://scenes/projectiles/LaserProjectile.tscn")
export var speed = 100

func _input(event):
	if is_network_master():
		if event.is_action_pressed("fire") && $CoolDown.is_stopped():
			fire_weapon()

func fire_weapon():
	var projectile = laser_projectile.instance()
	get_node("/root/World/Projectiles").add_child(projectile)
	if Settings.mouse_control:
		var rot = get_viewport().get_mouse_position()
		var camera = get_viewport().get_camera()
		var world_point = camera.project_position(rot,1)
		
		projectile.velocity = get_parent().linear_velocity+(-get_parent().translation+world_point)*speed
		projectile.rotation = global_transform.looking_at(world_point,Vector3.UP).basis.get_euler()
	else:
		projectile.velocity = get_parent().linear_velocity-get_parent().transform.basis.z*speed
		projectile.rotation = global_transform.basis.get_euler()
	projectile.parent=get_network_master()
	projectile.translation = get_parent().translation
	$CoolDown.start()
	rpc("fire_puppet",projectile.velocity,projectile.rotation,projectile.parent,projectile.translation)

func _process(delta):
	pass
	$Label.text = str(get_parent().linear_velocity)

puppet func fire_puppet(vel,rot,parent,trans):
	var projectile = laser_projectile.instance()
	get_node("/root/World/Projectiles").add_child(projectile)
	projectile.velocity = vel
	projectile.rotation = rot
	projectile.parent = parent
	projectile.translation = trans


func _on_CoolDown_timeout():
	if Input.is_action_pressed("fire"):
		fire_weapon()
