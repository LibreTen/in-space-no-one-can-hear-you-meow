extends Spatial

export var SERVER_PORT = 25565
export var MAX_PLAYERS = 5
var player_info = {}

var my_info = { name = "Random Alien" }

onready var ship_base = preload("res://PlayerShip.tscn")
onready var ui_base = preload("res://scenes/systems/UI.tscn")


func _input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().quit()

# Called when the node enters the scene tree for the first time.
func _ready():
	print("start")
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	
	#OS.window_fullscreen = true
	#Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _player_connected(id):
	# Called on both clients and server when a peer connects. Send my info to it.
	rpc_id(id, "register_player", my_info)
	print("player connected")

func _player_disconnected(id):
	print("player "+str(id)+" disconnected")
	player_info.erase(id) # Erase player from info.

func _connected_ok():
	print("connection successful")

func _server_disconnected():
	print("server disconnected")

func _connected_fail():
	print("failed to connect")

remote func register_player(info):
	# Get the id of the RPC sender.
	var id = get_tree().get_rpc_sender_id()
	# Store the info
	spawn_other_ship(id)
	player_info[id] = info

func network_start():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(SERVER_PORT, MAX_PLAYERS)
	get_tree().network_peer = peer
	$CanvasLayer/Multiplayer.hide()
	spawn_ship(Vector3.ZERO,"basic")

func network_connect():
	var SERVER_IP = $CanvasLayer/Multiplayer/IP.text
	var peer = NetworkedMultiplayerENet.new()
	var error = peer.create_client(SERVER_IP, SERVER_PORT)
	get_tree().network_peer = peer
	$CanvasLayer/Multiplayer.hide()
	spawn_ship(Vector3.ZERO,"basic")

func spawn_ship(start_pos,ship_name):
	var selfPeerID = get_tree().get_network_unique_id()
	
	# LOAD LEVEL HERE?
	
	var stats = load("res://custom_resources/ships/"+ship_name+".tres")
	var player_ship = ship_base.instance()
	player_ship.set_name(str(selfPeerID))
	player_ship.set_network_master(selfPeerID)
	add_child(player_ship)
	player_ship.translation = start_pos
	player_ship.linear_damp = stats.linear_damp
	player_ship.angular_damp = stats.angular_damp
	player_ship.accel_base = stats.accel_base
	player_ship.accel_booster = stats.accel_booster
	player_ship.accel = player_ship.accel_base
	var camera = player_ship.get_node("PilotCamera")
	camera.current = true
	
	var ui = ui_base.instance()
	camera.add_child(ui)
	ui.translation = Vector3.ZERO


func spawn_other_ship(id):
	var other_player_ship = ship_base.instance()
	other_player_ship.set_name(str(id))
	other_player_ship.set_network_master(id) # Will be explained later
	add_child(other_player_ship)
	other_player_ship.translation = Vector3.ZERO


func _on_Host_pressed():
	network_start()


func _on_Connect_pressed():
	network_connect()

