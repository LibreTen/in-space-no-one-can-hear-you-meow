extends RigidBody

var accel_base = 200
var accel_booster = 3

var translational_motion := Vector3.ZERO
var rotational_motion := Vector3.ZERO
var accel = 200
var rot_accel = 100
var hull = 100


# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	#add_force(Vector3(100,0,0),Vector3.ZERO)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if is_network_master():
		rpc_unreliable("update_stats", translation,rotation)
		movement(delta)
		rotate_ship(delta)
	#add_force(transform.basis,Vector3.ZERO)
	

master func rotate_ship(delta):
	var rot
	var rotx
	var roty
	
	if Settings.mouse_control && OS.is_window_focused():
		rot = get_viewport().get_mouse_position()/get_viewport().size - Vector2(0.5,0.5)
		rotx = -rot.y*transform.basis.x
		roty = -rot.x*transform.basis.y
	else:
		roty = Input.get_action_strength("rotate_right")-Input.get_action_strength("rotate_left")
		roty *=-transform.basis.y
		rotx = Input.get_action_strength("rotate_up")-Input.get_action_strength("rotate_down")
		rotx *=transform.basis.x
	
	var rotz = Input.get_action_strength("rotate_clockwise")-Input.get_action_strength("rotate_rclockwise")
	rotz *=-transform.basis.z
	add_torque((roty+rotx+rotz)*delta*rot_accel)
	#add_torque(Vector3(10,0,0))

master func movement(delta):
	if Input.is_action_just_pressed("booster"):
		accel = accel_base * accel_booster
	if Input.is_action_just_released("booster"):
		accel = accel_base
	
	var movx = Input.get_action_strength("right")-Input.get_action_strength("left")
	var movy = Input.get_action_strength("up")-Input.get_action_strength("down")
	var movz = Input.get_action_strength("backward")-Input.get_action_strength("forward")
	
	var accelx = movx*transform.basis.x
	var accely = movy*transform.basis.y
	var accelz = movz*transform.basis.z
	add_force((accelz+accelx+accely)*delta*accel,Vector3.ZERO)

puppet func update_hull(value):
	if is_network_master():
		$PilotCamera/UI.update_hull(value)
	hull = value

func damage(dmg):
	update_hull(hull-dmg)

puppet func update_stats(pos,rot):
	translation = pos
	rotation = rot
